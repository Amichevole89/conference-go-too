import pika
import json
from .api_views import PresentationDetailEncoder


def send_presentation_to_queue(presentation, queue_name):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host="rabbitmq")
    )
    channel = connection.channel()
    channel.queue_declare(queue=queue_name)
    message = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    body = json.dumps(message, cls=PresentationDetailEncoder)
    channel.basic_publish(
        exchange="",
        routing_key=queue_name,
        body=body,
    )


def send_presentation_approval(presentation):
    send_presentation_to_queue(presentation, "presentation_rejections")


def send_presentation_approval(presentation):
    send_presentation_to_queue(presentation, "presentation_approval")
