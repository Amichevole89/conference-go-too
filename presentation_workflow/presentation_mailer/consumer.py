import json, pika, django, os, sys, time
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approvals(ch, method, properties, body):
    process_message()
    send()


def process_rejections(ch, method, properties, body):
    process_message()
    send()


def process_message():
    mail = json.loads(body)
    name = mail["presenter_name"]
    title = mail["title"]
    email = mail["presenter_email"]


def send():
    if process_approvals:
        decision = "accepted"
    else:
        decision = "rejected"
    send_mail(
        f"Your presentation has been {decision}",
        f"{name}, we're happy to tell you that your presentation {title} has been {decision}",
        "admin@conference.go",
        [f"{email}"],
        fail_silently=True,
    )


def consumer_message(queue_name, callback):
    channel.queue_declare(queue=queue_name)
    channel.basic_consume(
        queue=queue_name,
        on_message_callback=callback,
        auto_ack=True,
    )


while True:
    try:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host="rabbitmq")
        )
        channel = connection.channel()
        consumer_message("presentation_approvals", process_approvals)
        consumer_message("presentation_rejections", process_rejections)
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)

    # mail = json.loads(body)
    # name = mail["presenter_name"]
    # title = mail["title"]
    # email = mail["presenter_email"]
